# -*- coding: utf-8 -*-
import os
import traceback
from functools import reduce

import pandas as pd

from constants import EFKitConstants as constants


# logger
def logData(handler, data_dict):
    for k, v in data_dict.items():
        handler.addData(k, v)
    handler.nextEntry()


# descriptives


# load data
def load_data(prefix, path=None, colnames=None):
    if path is None:
        DATADIR = constants.DATADIR
    else:
        DATADIR = path

    ALLFILES = os.listdir(DATADIR)
    # check prefix
    FILENAMES = [i for i in ALLFILES if prefix in i]
    # get currect path
    FILENAMES = [os.path.join(DATADIR, f) for f in FILENAMES]
    # read all data into a DataFrame
    if colnames is None:
        df = pd.concat((pd.read_csv(f, header=0) for f in FILENAMES))
    else:
        df = pd.concat((pd.read_csv(
            f, header=0, names=colnames, index_col=False) for f in FILENAMES))
    df = pd.DataFrame(df)
    # return the DataFrame
    return df


def nogo_desp(df):
    # Organzize the data
    # check if the directory exists
    SUMDIR = os.path.join(constants.ROOTDIR, 'summaries')
    if not os.path.isdir(SUMDIR):
        os.mkdir(SUMDIR)
    # only use experiment data
    df = df[(df['running'] == 'experiment')]
    # group by subjects and stimuli
    by_stim = df.groupby(['stimulus', 'subject_nr'])
    # get accuracy
    m_acc = by_stim['acc'].mean().to_frame()
    m_acc.reset_index(inplace=True)
    # long to wide
    w_m_acc = m_acc.pivot(index='subject_nr', columns='stimulus', values='acc')
    w_m_acc.reset_index(inplace=True)
    # get mean RT
    # only use correct response
    cor_df = df[(df['acc'] == 1)]
    cor_df['RT'] = pd.to_numeric(cor_df['RT'], errors='coerce')
    by_stim_c = cor_df.groupby(['stimulus', 'subject_nr'])
    m_RT = by_stim_c['RT'].mean().to_frame()
    m_RT.reset_index(inplace=True)
    # long to wide
    w_m_RT = m_RT.pivot(index='subject_nr', columns='stimulus', values='RT')
    w_m_RT.reset_index(inplace=True)
    # combine them
    summary = pd.merge(
        w_m_acc, w_m_RT, on='subject_nr', suffixes=('_ACC', '_RT'))
    # remove no-go RT
    summary = summary.drop('nogo_RT', axis=1)
    # remove the annoying name of columns
    summary.columns.name = ''
    # difference between go and no-go
    failed_nogo = df[(df['stimulus'] == 'nogo') & (df['acc'] == 0)]
    failed_nogo['RT'] = pd.to_numeric(failed_nogo['RT'], errors='coerce')
    m_RT_nogo = failed_nogo['RT'].mean()
    # add dif between freq-go and No-go to the summary
    summary = summary.assign(
        diff_f_nogo=summary['frequent_go_RT'] - m_RT_nogo,
        diff_inf_nogo=summary['infrequent_go_RT'] - m_RT_nogo)
    # round the values
    summary = summary.round(3)
    # write to an excel file
    writer = pd.ExcelWriter(os.path.join(SUMDIR, 'NOGO_descriptives.xlsx'))
    summary.to_excel(writer, 'summary', encoding='utf-8', index=False)

    # Group level
    # change subject_nr into type category
    summary['subject_nr'] = summary['subject_nr'].astype('category')
    # get all means in a DataFrame
    means = summary.mean(numeric_only=True).to_frame()
    means.reset_index(inplace=True)
    stds = summary.std(numeric_only=True).to_frame()
    stds.reset_index(inplace=True)
    means.columns = ['DV', 'mean']
    stds.columns = ['DV', 'std']
    # round the values
    means = means.round(3)
    stds = stds.round(3)
    # write to the excel file
    means.to_excel(writer, 'means', encoding='utf-8', index=False)
    stds.to_excel(writer, 'stds', encoding='utf-8', index=False)
    # close the file
    writer.close()


def rvp_desp(df):
    # Organzize the data
    # check if the directory exists
    SUMDIR = os.path.join(constants.ROOTDIR, 'summaries')
    if not os.path.isdir(SUMDIR):
        os.mkdir(SUMDIR)
    # only use experiment data
    df = df[(df['running'] == 'experiment')]
    df['acc'] = df['acc'].astype('float')
    by_block = df.groupby(['subject_nr', 'block', 'targetShow'])
    # get mean accuracy
    m_acc = by_block['acc'].mean().to_frame()
    # get mean RT of corret trials
    cor_df = df[(df['targetShow'] == 1) & (df['acc'] == 1)]
    cor_df['RT'] = cor_df['RT'].astype('float')
    m_RT = cor_df.groupby(['subject_nr', 'block'])['RT'].mean().to_frame()
    m_RT.reset_index(inplace=True)
    # write to an excel file
    writer = pd.ExcelWriter(os.path.join(SUMDIR, 'RVP_descriptives.xlsx'))
    m_acc.to_excel(writer, 'mean_acc', encoding='utf-8')
    m_RT.to_excel(writer, 'mean_RT', encoding='utf-8', index=False)
    # check false alarms, misses and hits
    if all(df[['hit', 'miss', 'falseAlarm']].max() > 1):
        # old files
        fmh = df.groupby(['subject_nr',
                          'block'])[['hit', 'miss', 'falseAlarm']].max()
    else:
        # new files
        fmh = df.groupby(['subject_nr',
                          'block'])[['hit', 'miss', 'falseAlarm']].sum()
    # write to an excel files
    fmh.to_excel(writer, 'signalDetection', encoding='utf-8')

    # Group level
    # acc
    summary_acc = m_acc.groupby(['block'])['acc'].mean().to_frame()
    summary_acc.reset_index(inplace=True)
    # RT
    summary_RT = m_RT.groupby(['block'])['RT'].mean().to_frame()
    summary_RT.reset_index(inplace=True)
    # fmh
    summary_fmh = fmh.groupby(['block']).mean()
    summary_fmh.reset_index(inplace=True)
    # combine them
    summaries = [summary_acc, summary_RT, summary_fmh]
    summary = reduce(lambda left, right: pd.merge(left, right, on='block'),
                     summaries)
    # round the values
    summary = summary.round(3)
    # write to an excel file
    summary.to_excel(writer, 'group_means', encoding='utf-8')
    # close the file
    writer.close()


def swm_desp(df):
    # Organzize the data
    # check if the directory exists
    SUMDIR = os.path.join(constants.ROOTDIR, 'summaries')
    if not os.path.isdir(SUMDIR):
        os.mkdir(SUMDIR)
    max_stage = df.groupby(['subject_nr'])['boxNum'].max()
    # write to an excel file
    writer = pd.ExcelWriter(os.path.join(SUMDIR, 'SWM_descriptives.xlsx'))
    max_stage.to_excel(writer, 'max_stage', encoding='utf-8')
    # means of each difficulty level
    by_stage = df.groupby(['subject_nr', 'boxNum'])
    items = ['RT', 'clicks', 'between-search_errors', 'within-search_errors']
    # get all the means
    dfs = []
    for i in items:
        m = by_stage[i].mean().to_frame()
        m.reset_index(inplace=True)
        # long to wide
        m.pivot(index='subject_nr', columns='boxNum', values=i)
        dfs.append(m)
    # merge them
    df_final = reduce(lambda left, right:
                      pd.merge(left, right, how='outer',
                               on=['subject_nr', 'boxNum']), dfs)
    df_final = df_final.round(3)
    # write to the excel file
    df_final.to_excel(writer, 'by_stage_means', encoding=False, index=False)

    # Group level
    idx = ['RT', 'clicks', 'between-search_errors', 'within-search_errors']
    summary_stage = df_final.groupby(['boxNum'])[idx].mean()
    summary_stage.reset_index(inplace=True)
    # round the values
    summary_stage = summary_stage.round(3)
    # write to the excel file
    summary_stage.to_excel(
        writer, 'group_means', encoding='utf-8', index=False)
    # close the file
    writer.close()


def tol_desp(df):
    # check if the directory exists
    SUMDIR = os.path.join(constants.ROOTDIR, 'summaries')
    if not os.path.isdir(SUMDIR):
        os.mkdir(SUMDIR)
    # only use experiment data
    df = df[(df['running'] == 'experiment')]
    # change subject_nr into type category
    df['subject_nr'] = df['subject_nr'].astype('category')
    # get individual performance
    by_subj = df.groupby(['subject_nr', 'trial'])[['success']].sum()
    by_subj.reset_index(inplace=True)
    by_subj = by_subj.pivot(
        index='subject_nr', columns='trial', values='success').reset_index()
    # rename the columns
    by_subj = by_subj.add_prefix('trial_')
    by_subj = by_subj.rename(
        {
            'trial_subject_nr': 'subject_nr'
        }, axis='columns')
    # round the value
    by_subj = by_subj.round(3)
    # write to an excel file
    writer = pd.ExcelWriter(os.path.join(SUMDIR, 'TOL_descriptives.xlsx'))
    by_subj.to_excel(
        writer, 'by_subject_performance', encoding='utf-8', index=False)
    # get the number of violation by subject
    df['violation'] = df['violation'].astype('category')
    vios = df.groupby(['subject_nr'])['violation'].value_counts().to_frame()
    vios.columns = ['counts']
    vios.reset_index(inplace=True)
    # long to wide
    w_vios = vios.pivot(
        index='subject_nr', columns='violation', values='counts')
    # remove the name of columns
    w_vios.columns.name = ''
    w_vios.reset_index(inplace=True)
    # fill NaN with 0
    viotype = ['None', 'movement', 'time']
    for vio in viotype:
        if vio in w_vios.columns:
            w_vios[[vio]] = w_vios[[vio]].fillna(0)
    # write to the file
    w_vios.to_excel(
        writer, 'by_subject_violations', encoding='utf-8', index=False)

    # Group level analysis
    by_trial = df.groupby(['trial'])
    # get all the means and stds by trial
    items = [
        'success',
        'movement',
        'movements',  # for compatitability
        'first_movement_time',
        'completeRT'
    ]
    dfs_m = []
    dfs_s = []
    for i in items:
        # get means
        try:
            m = by_trial[i].mean().to_frame()
            m.reset_index(inplace=True)
            dfs_m.append(m)
            # get stds
            std = by_trial[i].std().to_frame()
            std.reset_index(inplace=True)
            dfs_s.append(std)
        except:
            pass
    # merge them
    df_final_mean = reduce(
        lambda left, right: pd.merge(left, right, how='outer', on=['trial']),
        dfs_m)
    df_final_std = reduce(
        lambda left, right: pd.merge(left, right, how='outer', on=['trial']),
        dfs_s)
    # remove the meaningless std of success
    del df_final_std['success']
    by_trial_summary = pd.merge(
        df_final_mean, df_final_std, on='trial', suffixes=('_mean', '_std'))
    by_trial_summary = by_trial_summary.round(3)
    # write to the file
    by_trial_summary.to_excel(
        writer, 'by_trial_performance', encoding='utf-8', index=False)
    # close the file
    writer.close()


def wcst_desp(df):
    # check if the directory exists
    SUMDIR = os.path.join(constants.ROOTDIR, 'summaries')
    if not os.path.isdir(SUMDIR):
        os.mkdir(SUMDIR)
    # only use experiment data
    df = df[(df['running'] == 'experiment')]
    # fix mFail
    df['success_diff'] = df['success'] - df['success'].shift(1)
    df['mFail_corrected'] = df.eval("success_diff < -2 & success_diff > -6 \
                                    & acc == 0")
    # evaluate by-subject performances
    sum_items = ['otherError', 'MError', 'NError', 'mFail_corrected']
    sums = df.groupby(['subject_nr'])[sum_items].sum()
    sums.reset_index(inplace=True)
    maxes = df.groupby(['subject_nr'])['shift'].max().to_frame()
    maxes.reset_index(inplace=True)
    # RT
    m_RT = df.groupby(['subject_nr'])['RT'].mean().to_frame()
    m_RT.reset_index(inplace=True)
    m_RT.columns = ['subject_nr', 'm_RT']
    m_RT = m_RT.round(3)
    # merge them
    df_final = reduce(lambda left, right:
                      pd.merge(left, right, how='outer', on=['subject_nr']),
                      [sums, maxes, m_RT])
    # write to an excel file
    writer = pd.ExcelWriter(os.path.join(SUMDIR, 'WCST_descriptives.xlsx'))
    df_final.to_excel(
        writer, 'by-subject_performance', index=False, encoding='utf-8')

    # Group level analysis
    # change subject_nr into type category
    df_final['subject_nr'] = df_final['subject_nr'].astype('category')
    # means
    means = df_final.mean(numeric_only=True).to_frame()
    means = means.round(3)
    means.reset_index(inplace=True)
    means.columns = ['DV', 'mean']
    # write to the excel file
    means.to_excel(writer, 'group_means', encoding='utf-8', index=False)
    # close the file
    writer.close()

# -*- coding: utf-8 -*-
import os
import sys
import traceback

from PyQt5 import QtCore, QtGui, QtWidgets

import utility as ut


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1050, 1000)
        MainWindow.setMinimumSize(QtCore.QSize(1050, 1000))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap("EFKit.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.data_label = QtWidgets.QLabel(self.centralwidget)
        self.data_label.setGeometry(QtCore.QRect(25, 30, 150, 100))
        self.data_label.setMinimumSize(QtCore.QSize(150, 100))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.data_label.setFont(font)
        self.data_label.setObjectName("data_label")
        self.verticalWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalWidget.setGeometry(QtCore.QRect(30, 130, 1000, 631))
        self.verticalWidget.setObjectName("verticalWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.task_label = QtWidgets.QLabel(self.verticalWidget)
        self.task_label.setMinimumSize(QtCore.QSize(1000, 50))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(14)
        self.task_label.setFont(font)
        self.task_label.setObjectName("task_label")
        self.verticalLayout.addWidget(self.task_label)
        self.nogo_check = QtWidgets.QCheckBox(self.verticalWidget)
        self.nogo_check.setMinimumSize(QtCore.QSize(1000, 80))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.nogo_check.setFont(font)
        self.nogo_check.setObjectName("nogo_check")
        self.verticalLayout.addWidget(self.nogo_check)
        self.rvp_check = QtWidgets.QCheckBox(self.verticalWidget)
        self.rvp_check.setMinimumSize(QtCore.QSize(1000, 80))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.rvp_check.setFont(font)
        self.rvp_check.setObjectName("rvp_check")
        self.verticalLayout.addWidget(self.rvp_check)
        self.swm_check = QtWidgets.QCheckBox(self.verticalWidget)
        self.swm_check.setMinimumSize(QtCore.QSize(1000, 80))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.swm_check.setFont(font)
        self.swm_check.setObjectName("swm_check")
        self.verticalLayout.addWidget(self.swm_check)
        self.wcst_check = QtWidgets.QCheckBox(self.verticalWidget)
        self.wcst_check.setMinimumSize(QtCore.QSize(1000, 80))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.wcst_check.setFont(font)
        self.wcst_check.setObjectName("wcst_check")
        self.verticalLayout.addWidget(self.wcst_check)
        self.tol_check = QtWidgets.QCheckBox(self.verticalWidget)
        self.tol_check.setMinimumSize(QtCore.QSize(1000, 80))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.tol_check.setFont(font)
        self.tol_check.setObjectName("tol_check")
        self.verticalLayout.addWidget(self.tol_check)
        self.run_but = QtWidgets.QPushButton(self.verticalWidget)
        self.run_but.setMinimumSize(QtCore.QSize(500, 50))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        self.run_but.setFont(font)
        self.run_but.setObjectName("run_but")
        self.verticalLayout.addWidget(self.run_but)
        self.verticalWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.verticalWidget_2.setGeometry(QtCore.QRect(30, 780, 1000, 150))
        self.verticalWidget_2.setMinimumSize(QtCore.QSize(1000, 150))
        self.verticalWidget_2.setObjectName("verticalWidget_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.verticalWidget_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.copy_NTU = QtWidgets.QLabel(self.verticalWidget_2)
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(11)
        self.copy_NTU.setFont(font)
        self.copy_NTU.setObjectName("copy_NTU")
        self.verticalLayout_2.addWidget(self.copy_NTU)
        self.copy_authors = QtWidgets.QLabel(self.verticalWidget_2)
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(11)
        self.copy_authors.setFont(font)
        self.copy_authors.setObjectName("copy_authors")
        self.verticalLayout_2.addWidget(self.copy_authors)
        self.data_path = QtWidgets.QTextEdit(self.centralwidget)
        self.data_path.setGeometry(QtCore.QRect(180, 60, 850, 40))
        self.data_path.setMinimumSize(QtCore.QSize(500, 40))
        self.data_path.setObjectName("data_path")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1050, 30))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "EF Describer"))
        self.data_label.setText(_translate("MainWindow", "Data folder:"))
        self.task_label.setText(_translate("MainWindow", "Task:"))
        self.nogo_check.setText(_translate("MainWindow", "No-go (NOGO)"))
        self.rvp_check.setText(
            _translate("MainWindow", "Rapid visual processing (RVP)"))
        self.swm_check.setText(
            _translate("MainWindow", "Spatial working memory (SWM)"))
        self.wcst_check.setText(
            _translate("MainWindow", "Wisconsin card sorting (WCST)"))
        self.tol_check.setText(
            _translate("MainWindow", "Tower of London (TOL)"))
        self.run_but.setText(_translate("MainWindow", "Run"))
        self.copy_NTU.setText(
            _translate(
                "MainWindow",
                "Created by NTU Baby Lab. Released under GPL-3.0-or-later"))
        self.copy_authors.setText(
            _translate("MainWindow",
                       "© Copyright 2018 Yu-Han Luo & Feng-Ming Tsao"))


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        SCRIPTDIR = os.path.abspath(os.path.dirname(__file__))
        ROOTDIR = os.path.abspath(os.path.join(SCRIPTDIR, os.pardir))
        DATADIR = os.path.join(ROOTDIR, 'data')
        self.data_path.setText(DATADIR)
        self.msg = QtWidgets.QMessageBox(self)
        self.msg.setWindowTitle("EF Kit")
        self.run_but.clicked.connect(self.run)

    def process_nogo(self):
        try:
            df = ut.load_data('NOGO',path=self.DATADIR)
            ut.nogo_desp(df)
        except:
            with open('nogo_log.txt', 'w') as f:
                f.write(traceback.format_exc())
                f.close()

    def process_rvp(self):
        try:
            df = ut.load_data(
                'RVP',
                colnames=[
                    'subject_nr', 'running', 'block', 'trials',
                    'duration_stimulus', 'RT', 'acc', 'targetShow',
                    'mouse_response', 'hit', 'miss', 'falseAlarm',
                    'experiment_date'
                ],path=self.DATADIR)
            ut.rvp_desp(df)
        except:
            with open('rvp_log.txt', 'w') as f:
                f.write(traceback.format_exc())
                f.close()

    def process_swm(self):
        try:
            df = ut.load_data('SWM',path=self.DATADIR)
            ut.swm_desp(df)
        except:
            with open('swm_log.txt', 'w') as f:
                f.write(traceback.format_exc())
                f.close()

    def process_tol(self):
        try:
            df = ut.load_data('TOL',path=self.DATADIR)
            ut.tol_desp(df)
        except:
            with open('tol_log.txt', 'w') as f:
                f.write(traceback.format_exc())
                f.close()

    def process_wcst(self):
        try:
            df = ut.load_data('WCST',path=self.DATADIR)
            ut.wcst_desp(df)
        except:
            with open('wcst_log.txt', 'w') as f:
                f.write(traceback.format_exc())
                f.close()

    def start_process(self):
        # if all tasks are unselected, raise an error message.
        tasks_check = [
            self.nogo_check.isChecked(),
            self.rvp_check.isChecked(),
            self.swm_check.isChecked(),
            self.wcst_check.isChecked(),
            self.tol_check.isChecked()
        ]
        num_tasks = sum(i for i in tasks_check)

        if num_tasks == 0:
            self.msg.setIcon(QtWidgets.QMessageBox.Warning)
            self.msg.setText('None of the task was selected.')
            self.msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            self.msg.exec_()
        else:
            try:
                if self.nogo_check.isChecked():
                    self.process_nogo()

                if self.rvp_check.isChecked():
                    self.process_rvp()

                if self.swm_check.isChecked():
                    self.process_swm()

                if self.wcst_check.isChecked():
                    self.process_wcst()

                if self.tol_check.isChecked():
                    self.process_tol()

                # show a information window if finish processing
                self.msg.setIcon(QtWidgets.QMessageBox.Information)
                self.msg.setText('Finished successfully!')
                self.msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                self.msg.exec_()

            except ValueError:
                self.msg.setIcon(QtWidgets.QMessageBox.Warning)
                self.msg.setText('Unable to find the data.')
                self.msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                self.msg.exec_()

    def run(self):
        self.DATADIR = self.data_path.toPlainText()
        self.start_process()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    app.exit(app.exec_())
    # delete the widget manually...
    del window.msg

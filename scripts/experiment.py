# -*- coding: utf-8 -*-
import math
from itertools import product

import numpy as np
import os
from psychopy import core, data, event, visual
from scipy.spatial.distance import cdist

import constants
from utility import logData


class Nogo:
    def __init__(self, subject_nr):
        self.constants = constants.NogoConstants()
        self.info = {
            'subject_nr': subject_nr,
            'experiment_date': self.constants.DATE
        }

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=self.constants.DISPSIZE,
            units='pix',
            color=self.constants.BGC,
            fullscr=True,
            allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.mouse.setVisible(False)
        # create Clock for timer
        self.timer = core.Clock()

        # instructions
        self.instStim = visual.ImageStim(self.win, image=self.constants.INST)
        self.pracEnd0 = visual.ImageStim(
            self.win, image=self.constants.PRACEND0)
        self.pracEnd1 = visual.ImageStim(
            self.win, image=self.constants.PRACEND1)
        self.conBut = visual.ImageStim(
            self.win, image=self.constants.CON, pos=(0, -700))
        self.bye = visual.ImageStim(self.win, image=self.constants.BYE)
        # fixation
        self.fix = visual.TextStim(
            self.win, text='+', height=self.constants.FIXSIZE)
        # go stimulus
        self.freq_go = visual.ImageStim(self.win, image=self.constants.FREQGO)
        self.infreq_go = visual.ImageStim(
            self.win, image=self.constants.INFREQGO)
        # no-go stimulus
        self.nogo = visual.ImageStim(self.win, image=self.constants.NOGO)
        self.stims = {
            'frequent_go': self.freq_go,
            'infrequent_go': self.infreq_go,
            'nogo': self.nogo
        }
        # feedbacks
        self.fb = {}
        self.fb[0] = visual.ImageStim(
            self.win, image=self.constants.INCORRECT, pos=(-600, 0))
        self.fb[1] = visual.ImageStim(
            self.win, image=self.constants.CORRECT, pos=(-600, 0))
        # create trials
        self.all_trials = []
        for stim, number in self.constants.STIMS.items():
            trial = {}
            trial['stim'] = stim
            self.all_trials.extend(number * [trial])
        # randomization
        np.random.shuffle(self.all_trials)
        self.prac_trials = []
        for stim, number in self.constants.STIMS.items():
            trial = {}
            trial['stim'] = stim
            self.prac_trials.extend(
                int(round(self.constants.PRACRATIO * number)) * [trial])
        # randomization
        np.random.shuffle(self.prac_trials)
        # data file
        self.logfile = data.ExperimentHandler(
            name='NOGO_subject-{}'.format(self.info['subject_nr']),
            extraInfo=self.info,
            autoLog=False)

    def run(self):
        # generate random seed
        np.random.seed()
        # prepare the experiment
        self.prepare()

        while True:
            # show instructions
            self.instStim.draw()
            self.win.flip()
            if any(self.mouse.getPressed()):
                break

        self.practice()
        self.experiment()

        # show ending of the experiment
        while True:
            self.bye.draw()
            self.win.flip()
            if any(self.mouse.getPressed()):
                break
        # end the experiment
        self.logfile.close()
        self.logfile.saveAsWideText(
            fileName=os.path.join(
                constants.EFKitConstants.DATADIR,
                'NOGO_subject-{}.csv'.format(self.info['subject_nr'])),
            delim=',')
        self.win.close()

    # practice
    def practice(self):
        again = True
        while again:
            performance = []
            count = 0
            for trial in self.prac_trials:
                count += 1
                trial_data = self.trial_seq('practice', trial)
                performance.append(trial_data['acc'])
                trial_data['trials'] = count
                logData(self.logfile, trial_data)

            # calculate the accuracy
            m_acc = np.mean(performance)
            m_accStim = visual.TextStim(
                self.win,
                text='{}%'.format(str(m_acc * 100)),
                pos=(-300, -150),
                height=120)

            # show ending of practice
            if m_acc >= self.constants.THRESHOLD:
                # end of practice
                # reset the mouse position
                self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
                self.timer.reset()
                while self.timer.getTime() <= 15:
                    self.pracEnd1.draw()
                    m_accStim.draw()
                    self.conBut.draw()
                    self.win.flip()
                    mouse_pos = self.mouse.getPos()
                    if cdist([mouse_pos], [[0, -700]]) <= 168:
                        again = False
                        break

            else:
                self.pracEnd0.draw()
                m_accStim.draw()
                self.win.flip()
                core.wait(15)

    # experiment trials
    def experiment(self):
        count = 0
        for trial in self.all_trials:
            count += 1
            trial_data = self.trial_seq('experiment', trial)
            trial_data['trials'] = count
            logData(self.logfile, trial_data)

    # create a single trial sequence
    def trial_seq(self, running, trial):
        # show fixation
        self.fix.draw()
        self.win.flip()
        core.wait(self.constants.FIXTIME)
        # show stimulus
        self.stims[trial['stim']].draw()
        self.win.flip()

        # wait for response
        self.timer.reset()
        self.mouse.clickReset()
        while self.timer.getTime() <= self.constants.STIMTIME:
            buttons, times = self.mouse.getPressed(getTime=True)
            # valid RT should be larger than 100ms
            if buttons[0] and times[0] >= 0.1:
                RT = round(times[0], 4)
                # check acc
                if trial['stim'] is not 'nogo':
                    acc = 1
                else:
                    acc = 0
                break
        else:
            RT = None
            if trial['stim'] is 'nogo':
                acc = 1
            else:
                acc = 0

        # show feedback for practice trials
        if running == 'practice':
            self.stims[trial['stim']].draw()
            self.fb[acc].draw()

        # log data
        trial_data = {
            'running': running,
            'stimulus': trial['stim'],
            'acc': acc,
            'RT': RT
        }
        # ITI
        self.win.flip()
        core.wait(self.constants.ITI)
        return trial_data


class RVP:
    def __init__(self, subject_nr):
        self.constants = constants.RVPConstants()
        self.info = {
            'subject_nr': subject_nr,
            'experiment_date': self.constants.DATE
        }

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=self.constants.DISPSIZE,
            units='pix',
            color=self.constants.BGC,
            fullscr=True,
            allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.mouse.setVisible(False)
        # create Clock for timer
        self.timer = core.Clock()

        # stimuli
        ch_keys = [x for x in self.constants.NUMBERS]
        self.numbers = dict.fromkeys(ch_keys, None)
        for k, v in self.numbers.items():
            v = visual.TextStim(
                self.win,
                text=k,
                color=self.constants.FGC,
                height=self.constants.FONTSIZE)
            self.numbers[k] = v

        # instructions
        self.instStim = visual.ImageStim(self.win, image=self.constants.INST)
        self.pracEnd = visual.ImageStim(self.win, image=self.constants.PRACEND)
        self.resting = visual.ImageStim(self.win, image=self.constants.REST)
        self.conBut = visual.ImageStim(
            self.win, image=self.constants.CON, pos=(0, -700))
        self.bye = visual.ImageStim(self.win, image=self.constants.BYE)
        self.mouse_icon = visual.ImageStim(
            self.win, image=self.constants.MOUSE, pos=(-900, 500))
        # feedbacks
        self.fb = {}
        self.fb[0] = visual.ImageStim(
            self.win, image=self.constants.INCORRECT, pos=(-900, 0))
        self.fb[1] = visual.ImageStim(
            self.win, image=self.constants.CORRECT, pos=(-900, 0))
        # data file
        self.logfile = data.ExperimentHandler(
            name='RVP_subject-{}'.format(self.info['subject_nr']),
            extraInfo=self.info,
            autoLog=False)

    def run(self):
        # generate random seed
        np.random.seed()
        # prepare the experiment
        self.prepare()
        # show instructions
        self.instStim.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break

        self.practice()

        # block 1
        self.experiment()
        # reseting 1
        while True:
            self.resting.draw()
            self.conBut.draw()
            self.win.flip()
            mouse_pos = self.mouse.getPos()
            if cdist([mouse_pos], [[0, -700]]) <= 168:
                again = False
                break

        # block 2
        self.experiment(block=2)
        # reseting 2
        while True:
            self.resting.draw()
            self.conBut.draw()
            self.win.flip()
            mouse_pos = self.mouse.getPos()
            if cdist([mouse_pos], [[0, -700]]) <= 168:
                again = False
                break
        # block 3
        self.experiment(block=3)

        # show ending of the experiment
        self.bye.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        # end the experiment
        self.logfile.close()
        self.logfile.saveAsWideText(
            fileName=os.path.join(
                constants.EFKitConstants.DATADIR,
                'RVP_subject-{}.csv'.format(self.info['subject_nr'])),
            delim=',')
        self.win.close()

    def practice(self, block=1):
        # reset the mouse position to hide it
        self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
        again = True
        while again:
            all_seq = self.gen_seq(self.constants.SEQNUM)
            # decide target
            tar_seq = np.random.choice(
                range(self.constants.SEQNUM - 1),
                size=self.constants.TARNUM - 1,
                replace=False)
            tar_seq = np.append(tar_seq, 39)
            tar_seq.sort()
            tars = all_seq[tar_seq]

            # because it's practice, show the mouse icon
            self.mouse_icon.setAutoDraw(True)
            counts = 0
            # iterrate through rows
            for row in all_seq:
                # it's not the best solution, but it keeps track of the index.
                for i in range(3):
                    if np.array_equal(row, tars[0]) and i == 2:
                        targetShow = 1
                    else:
                        targetShow = 0
                    trial_data = self.trial_seq('practice', row[i], tars[0],
                                                targetShow)
                    trial_data['trials'] = counts
                    trial_data['block'] = block
                    logData(self.logfile, trial_data)
                    if targetShow:
                        # remove it
                        tars = np.delete(tars, 0, 0)
            # show the end of practice
            self.timer.reset()
            # reset the mouse position to hide it
            self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
            while self.timer.getTime() <= 15:
                self.pracEnd.draw()
                self.conBut.draw()
                self.win.flip()
                mouse_pos = self.mouse.getPos()
                if cdist([mouse_pos], [[0, -700]]) <= 168:
                    again = False
                    break
        # finish the practice
        self.mouse_icon.setAutoDraw(False)

    def experiment(self, block=1):
        # reset the mouse position to hide it
        self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
        all_seq = self.gen_seq(self.constants.SEQNUM)
        # decide target
        tar_seq = np.random.choice(
            range(self.constants.SEQNUM - 1),
            size=self.constants.TARNUM - 1,
            replace=False)
        tar_seq = np.append(tar_seq, 39)
        tar_seq.sort()
        tars = all_seq[tar_seq]

        counts = 0
        # iterrate through rows
        for row in all_seq:
            # it's not the best solution, but it keeps track of the index.
            for i in range(3):
                if np.array_equal(row, tars[0]) and i == 2:
                    targetShow = 1
                else:
                    targetShow = 0
                trial_data = self.trial_seq('experiment', row[i], tars[0],
                                            targetShow)
                trial_data['trials'] = counts
                trial_data['block'] = block
                logData(self.logfile, trial_data)
                if targetShow:
                    # remove it
                    tars = np.delete(tars, 0, 0)

    # create a single trial sequence
    def trial_seq(self, running, stim, tar, targetShow):
        hit = 0
        miss = 0
        falseAlarm = 0
        # draw stimuli
        self.numbers[stim].setPos([0, 0])
        self.numbers[stim].draw()
        # draw target
        self.numbers[tar[0]].setPos(
            [2 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[0]].draw()
        self.numbers[tar[1]].setPos(
            [3 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[1]].draw()
        self.numbers[tar[2]].setPos(
            [4 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[2]].draw()
        # show
        self.win.flip()
        self.timer.reset()
        self.mouse.clickReset()
        while self.timer.getTime() <= self.constants.STIMTIME:
            buttons, times = self.mouse.getPressed(getTime=True)
            if buttons[0] and times[0] >= 0.1:
                RT = times[0]
                click = 1
                if targetShow:
                    acc = 1
                    hit = 1
                else:
                    acc = 0
                    falseAlarm = 1
                break
        else:
            # timeout
            RT = None
            click = 0
            if targetShow:
                acc = 0
                miss = 1
            else:
                acc = 1

        # redraw everything
        # draw target
        self.numbers[tar[0]].setPos(
            [2 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[0]].draw()
        self.numbers[tar[1]].setPos(
            [3 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[1]].draw()
        self.numbers[tar[2]].setPos(
            [4 * self.constants.FONTSIZE, self.constants.FONTSIZE + 1])
        self.numbers[tar[2]].draw()
        # show feedback
        if running == 'practice':
            self.fb[acc].draw()
            self.win.flip()
            core.wait(0.3)
        else:
            self.win.flip()
            core.wait(0.1)

        trial_data = {
            'running': running,
            'RT': RT,
            'acc': acc,
            'targetShow': targetShow,
            'mouse_response': click,
            'hit': hit,
            'miss': miss,
            'falseAlarm': falseAlarm
        }
        return trial_data

    def gen_seq(self, n):
        all_seq = np.array([], dtype=int)
        nums = range(2, 10)
        for i in range(n**100):
            if len(all_seq) != 0:
                last = all_seq[-3:]
                seq = np.random.choice(
                    [x for x in nums if x not in last], size=3)
            else:
                seq = np.random.choice(nums, size=3)
            # check for duplicates
            try:
                repeats = all_seq[0, (all_seq[1:] == seq[:, None]).all(0)]
            except:
                all_seq = np.append(all_seq, seq)
            if all_seq.size == n * 3:
                break
        all_seq = all_seq.astype(int)
        all_seq = all_seq.reshape((n, 3))
        return all_seq


class SWM:
    def __init__(self, subject_nr):
        self.constants = constants.SWMConstants()
        self.info = {
            'subject_nr': subject_nr,
            'experiment_date': self.constants.DATE
        }

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=self.constants.DISPSIZE,
            units='pix',
            color=self.constants.BGC,
            fullscr=True,
            allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.mouse.setVisible(True)
        # create Clock for timer
        self.timer = core.Clock()

        # instructions
        self.instStim1 = visual.ImageStim(self.win, image=self.constants.INST1)
        self.instStim2 = visual.ImageStim(self.win, image=self.constants.INST2)
        self.instStim3 = visual.ImageStim(self.win, image=self.constants.INST3)
        self.pracEnd = visual.ImageStim(self.win, image=self.constants.PRACEND)
        self.bye = visual.ImageStim(self.win, image=self.constants.BYE)

        # stimuli
        self.coverPrac = visual.ImageStim(
            self.win,
            image=self.constants.COVERPRAC,
            size=self.constants.COVERSIZE)
        self.birdPrac = visual.ImageStim(
            self.win,
            image=self.constants.BIRDPRAC,
            size=self.constants.TARSIZE)
        self.coverTrees = [
            visual.ImageStim(self.win, image=x, size=self.constants.COVERSIZE)
            for x in self.constants.COVERTREES
        ]
        np.random.shuffle(self.coverTrees)
        self.unfoundLeaf = visual.ImageStim(
            self.win, image=self.constants.LEAF, size=self.constants.TARSIZE)
        self.tarBirds = [
            visual.ImageStim(self.win, image=x, size=self.constants.COVERSIZE)
            for x in self.constants.BIRDS
        ]
        np.random.shuffle(self.tarBirds)
        # hint
        self.constants.HINTY = self.hint_pos()
        self.hint = visual.TextStim(
            self.win, text='?', height=30, color=(-1, -1, -1))
        # create a column for indication of found birds
        self.column = visual.Rect(
            self.win,
            pos=(math.ceil(
                0.5 * (self.constants.DISPSIZE[0] - self.constants.WMARGIN)),
                 0),
            width=self.constants.WMARGIN,
            height=self.constants.DISPSIZE[1],
            lineColor=(-1, -1, -1))
        # define the limits of x and y position in advance
        xlim = range(
            math.ceil(
                0.5 *
                (self.constants.COVERSIZE[0] - self.constants.DISPSIZE[0]) +
                self.constants.WMARGIN + 1),
            math.ceil(
                0.5 *
                (self.constants.DISPSIZE[0] - self.constants.COVERSIZE[0]) -
                self.constants.WMARGIN + 1))  # to avoid overlapping with bar
        ylim = range(
            math.ceil(
                0.5 *
                (self.constants.COVERSIZE[1] - self.constants.DISPSIZE[1]) +
                self.constants.HMARGIN + 1),
            math.ceil(
                0.5 *
                (self.constants.DISPSIZE[1] - self.constants.COVERSIZE[1]) -
                self.constants.HMARGIN + 1))
        # get the huge x-y plane
        self.products = np.array(np.meshgrid(xlim, ylim)).T.reshape(-1, 2)
        # data file
        self.logfile = data.ExperimentHandler(
            name='SWM_subject-{}'.format(self.info['subject_nr']),
            extraInfo=self.info,
            autoLog=False)

    def run(self):
        # randomization
        np.random.seed()
        # prepare
        self.prepare()

        # show instruction
        self.instStim1.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        self.win.flip()
        core.wait(self.constants.FLIPTIME)

        # practice phase
        self.practice()
        # experiment
        self.experiment()

        # show ending of the experiment
        self.bye.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        # end the experiment
        self.logfile.close()
        self.logfile.saveAsWideText(
            fileName=os.path.join(
                constants.EFKitConstants.DATADIR,
                'SWM_subject-{}.csv'.format(self.info['subject_nr'])),
            delim=',')
        self.win.close()

    def practice(self):
        # further instruction
        self.instStim2.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        # phase 1
        self.coverPos = self.choose_pos(
            products=self.products, boxNum=1,
            min_dis=self.constants.MINRATIO * max(self.constants.COVERSIZE))
        # set mouse position to an impossible position for target to appear
        self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
        while True:
            mouse_pos = self.mouse.getPos()
            # draw covers
            self.coverPrac.setPos(self.coverPos)
            self.coverPrac.draw()
            self.win.flip()
            if cdist([mouse_pos],
                     self.coverPos) <= self.constants.COVERSIZE[0]:
                self.birdPrac.setPos(self.coverPos)
                self.birdPrac.draw()
                self.win.flip()
                core.wait(1)
                break

        self.win.flip()
        core.wait(1)

        # phase 2
        # practice for two times
        self.coverPos = self.choose_pos(
            products=self.products, boxNum=2,
            min_dis=self.constants.MINRATIO * max(self.constants.COVERSIZE))
        self.trial_seq(self.coverPrac, self.birdPrac)

        # phase 3
        for i in range(2):
            self.coverPos = self.choose_pos(
                products=self.products, boxNum=3,
                min_dis=self.constants.MINRATIO * max(self.constants.COVERSIZE))
            self.trial_seq(self.coverPrac, self.birdPrac)
        # show the end of practice
        self.pracEnd.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break

    def experiment(self):
        for lev in self.constants.BOXNUMS:
            cover_tree = self.coverTrees[self.constants.BOXNUMS.index(lev)]
            tar_bird = self.tarBirds[self.constants.BOXNUMS.index(lev)]
            for i in range(self.constants.TRIALREPEATS):
                # select position
                self.coverPos = self.choose_pos(
                    products=self.products, boxNum=int(lev),
                    min_dis=self.constants.MINRATIO * max(self.constants.COVERSIZE))
                trial_data = self.trial_seq(cover_tree, tar_bird)
                logData(self.logfile, trial_data)
                # check response time
                if trial_data['timeout']:
                    break
            else:
                continue # continue if the inner loop wasn't broken
            # inner loop was broken, break the outer loop
            break

    def trial_seq(self, cover_tree, tar_bird):
        self.cover_tree = cover_tree
        self.tar_bird = tar_bird
        # sub-trial order
        sub_trials = list(range(len(self.coverPos)))
        np.random.shuffle(sub_trials)
        self.target_found = [0] * len(self.coverPos)
        clicks = 0
        b_errors = 0
        w_errors = 0
        click_seq = []
        # create a list of index
        cover_visited = [0] * len(self.coverPos)
        # set mouse position to an impossible position
        self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
        # reset timer
        self.timer.reset()
        for sub_trial in sub_trials:
            working = True
            while working:
                self.update_hints()
                for p in self.coverPos:
                    self.cover_tree.setPos(p)
                    self.cover_tree.draw()
                self.win.flip()
                mouse_pos = self.mouse.getPos()
                for cover in self.coverPos:
                    i = self.array_index(self.coverPos, cover)
                    # check position
                    if cdist([mouse_pos],
                             [cover]) <= max(self.constants.COVERSIZE):
                        # increase the number of clicks
                        clicks += 1
                        # record the position
                        click_seq.append(i)
                        # check if it's target
                        if i == sub_trial:
                            # mark as found
                            self.target_found[i] = 1
                            self.tar_bird.setPos(cover)
                            self.tar_bird.draw()
                            others = self.coverPos[(self.coverPos !=
                                                    cover).all(axis=1)]
                            for p in others:
                                self.cover_tree.setPos(p)
                                self.cover_tree.draw()
                        # not target
                        else:
                            # check if visited in the current search
                            if not cover_visited[i]:
                                # mark it as visited
                                cover_visited[i] = 1
                            else:
                                # check if it has been found to be target
                                if self.target_found[i]:
                                    b_errors += 1
                                # visited and hasn't been found to be target
                                else:
                                    w_errors += 1
                            # unfound
                            self.unfoundLeaf.setPos(cover)
                            self.unfoundLeaf.draw()
                            others = self.coverPos[(self.coverPos !=
                                                    cover).all(axis=1)]
                            for p in others:
                                self.cover_tree.setPos(p)
                                self.cover_tree.draw()
                        # show the result
                        self.update_hints()
                        self.win.flip()
                        core.wait(self.constants.FLIPTIME)
                        # set mouse position to an impossible position
                        self.mouse.setPos(
                            [0.5 * x for x in self.constants.DISPSIZE])
                        # mark as visited
                        cover_visited[i] = 1

                if self.target_found[sub_trial]:
                    # end current search
                    working = False
        # finish
        RT = self.timer.getTime()
        # check if timeout
        if RT >= self.constants.TIMEOUT:
            timeout = 1
        else:
            timeout = 0
        click_seq = '{}'.format(str(click_seq))
        self.win.flip()
        core.wait(1)
        trial_data = {
            'boxNum': len(self.coverPos),
            'RT': RT,
            'between-search_errors': b_errors,
            'within-search_errors': w_errors,
            'clickSequence': click_seq,
            'clicks': clicks,
            'targetSequence': sub_trials,
            'timeout': timeout
        }
        return trial_data

    def choose_pos(self, products, boxNum, min_dis, niter=100000):
        for attempt in range(niter):
            idx = np.random.randint(products.shape[0], size=boxNum)
            out = products[idx]
            nrow = out.shape[0]
            checks = []
            for i in range(nrow):
                x = out[i]
                y = out[(out != x).all(axis=1)]
                d = cdist([x], y)
                if np.greater(d, min_dis).all():
                    checks.append(1)
                else:
                    checks.append(0)
            if all(checks):
                return out
        else:
            # fail
            raise RuntimeError(
                'Unable to find suitable positions. Use smaller cover image?')

    def array_index(self, array, row):
        i = (array == row).all(axis=1).nonzero()  # return tuple
        return i[0][0]

    def update_hints(self):
        self.column.draw()
        unfound_num = len(self.coverPos) - sum(self.target_found)
        for i in range(unfound_num):
            self.hint.setPos([
                0.5 * (self.constants.DISPSIZE[0] - self.constants.WMARGIN),
                self.constants.HINTY[i]
            ])
            self.hint.draw()
        for j in range(len(self.coverPos)):
            if j >= unfound_num:
                self.tar_bird.setSize([0.5 * x for x in self.tar_bird.size])
                self.tar_bird.setPos([
                    0.5 *
                    (self.constants.DISPSIZE[0] - self.constants.WMARGIN),
                    self.constants.HINTY[j]
                ])
                self.tar_bird.draw()
                # resume the size
                self.tar_bird.setSize([2 * x for x in self.tar_bird.size])

    def hint_pos(self):
        # define the position of hints
        y = np.linspace(self.constants.DISPSIZE[1] * 0.5 - 100,
                        self.constants.DISPSIZE[1] * (-0.5) + 100, 10)
        return [math.ceil(x) for x in y]


class TOL:
    def __init__(self, subject_nr):
        self.constants = constants.TOLConstants()
        self.info = {
            'subject_nr': subject_nr,
            'experiment_date': self.constants.DATE
        }

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=self.constants.DISPSIZE,
            units='pix',
            color=self.constants.BGC,
            fullscr=True,
            allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.mouse.setVisible(True)
        # create Clock for timer
        self.timer = core.Clock()

        # instructions
        self.instStim = visual.ImageStim(self.win, image=self.constants.INST)
        self.pracEnd = visual.ImageStim(self.win, image=self.constants.PRACEND)
        self.bye = visual.ImageStim(self.win, image=self.constants.BYE)
        self.conBut = visual.ImageStim(
            self.win, image=self.constants.CON, pos=(0, -700))

        self.button = visual.Circle(
            self.win,
            radius=self.constants.BUTSIZE,
            edges=60,
            lineColor=self.constants.BUTCOL,
            lineWidth=10)

        self.pegs = []
        # prepare pegs
        for h in self.constants.PEGHEIGHT:
            peg = visual.Rect(
                self.win,
                width=self.constants.PEGWIDTH,
                height=h,
                lineColor=self.constants.PEGCOL,
                fillColor=self.constants.PEGCOL)
            self.pegs.append(peg)

        # prepare ground
        self.ground = visual.Rect(
            self.win,
            width=self.constants.GROUNDWIDTH,
            height=self.constants.GROUNDHEIGHT,
            lineColor=self.constants.PEGCOL,
            fillColor=self.constants.PEGCOL)

        # prepare all beads
        self.subjAllBeads = {}
        for col in self.constants.BEADCOLS.keys():
            self.subjAllBeads[col] = visual.Circle(
                self.win,
                radius=self.constants.BEADR,
                edges=60,
                lineColor=self.constants.BEADCOLS[col],
                fillColor=self.constants.BEADCOLS[col])

        # prepare 'ring' on the chosen bead
        self.ring = visual.Circle(
            self.win,
            radius=self.constants.BEADR + 1,
            edges=60,
            lineWidth=10,
            lineColor=(1, 1, 1))

        # prepare feedback
        self.fb = {}
        self.fb[0] = visual.ImageStim(
            self.win, image=self.constants.FB0, pos=(-600, 0))
        self.fb[1] = visual.ImageStim(
            self.win, image=self.constants.FB1, pos=(-600, 0))
        self.fb[2] = visual.ImageStim(
            self.win, image=self.constants.FB2, pos=(-600, 0))

        # data file
        self.logfile = data.ExperimentHandler(
            name='TOL_subject-{}'.format(self.info['subject_nr']),
            extraInfo=self.info,
            autoLog=False)

    def run(self):
        # prepare
        self.prepare()
        # show instruction
        self.instStim.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break

        self.practice()
        self.experiment()
        # show ending of the experiment
        self.bye.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        # end the experiment
        self.logfile.close()
        self.logfile.saveAsWideText(
            fileName=os.path.join(
                constants.EFKitConstants.DATADIR,
                'TOL_subject-{}.csv'.format(self.info['subject_nr'])),
            delim=',')
        self.win.close()

    def practice(self):
        again = True
        while again:
            self.pegSeat = self.pegSeat_refresh(self.constants.PRACSTARTING)
            for idx, goal in self.constants.PRACTICE.items():
                self.goal = goal
                self.movement = 0
                first = True
                success = 0
                violation = None
                #trial start
                self.draw_pegs()
                self.draw_beads()
                self.win.flip()
                self.timer.reset()

                while True:
                    self.choosing_bead()
                    # waiting for movement
                    self.moving_bead()
                    # get first movement time
                    if first:
                        first_movement_time = self.timer.getTime()
                        first = False
                    # check if it is correct
                    if self.goal == self.pegSeat:
                        completeRT = self.timer.getTime()
                        success = 1
                        # log data
                        trial_data = {
                            'running': 'practice',
                            'trial': idx,
                            'success': success,
                            'movement': self.movement,
                            'first_movement_time': first_movement_time,
                            'completeRT': completeRT,
                            'violation': violation
                        }
                        logData(self.logfile, trial_data)
                        # clear the display
                        self.win.flip()
                        self.draw_pegs()
                        self.draw_beads()
                        self.fb[1].draw()
                        self.win.flip()

                        core.wait(self.constants.ITI)
                        break
                    else:
                        if self.movement < self.constants.MAXMOVE and self.timer.getTime(
                        ) < self.constants.MAXTIME:
                            pass
                        else:
                            # clear the display
                            self.win.flip()
                            if self.movement >= self.constants.MAXMOVE:
                                violation = 'movement'
                                self.fb[0].draw()
                            elif self.timer.getTime() >= self.constants.MAXTIME:
                                violation = 'time'
                                self.fb[2].draw()
                            completeRT = self.timer.getTime()
                            success = 0
                            # log data
                            trial_data = {
                                'running': 'practice',
                                'trial': idx,
                                'success': success,
                                'movement': self.movement,
                                'first_movement_time': first_movement_time,
                                'completeRT': completeRT,
                                'violation': violation
                            }
                            logData(self.logfile, trial_data)
                            self.draw_pegs()
                            self.draw_beads()
                            self.win.flip()
                            core.wait(self.constants.ITI)
                            self.pegSeat = self.goal
                            break
            # end of practice
            self.timer.reset()
            # reset the mouse position to hide it
            self.mouse.setPos([0.5 * x for x in self.constants.DISPSIZE])
            while self.timer.getTime() <= 15:
                self.pracEnd.draw()
                self.conBut.draw()
                self.win.flip()
                mouse_pos = self.mouse.getPos()
                if cdist([mouse_pos], [[0, -700]]) <= 168:
                    again = False
                    break

    def experiment(self):
        self.pegSeat = self.pegSeat_refresh(self.constants.STARTING)
        for idx, goal in self.constants.GOALS.items():
            self.goal = goal
            first = True
            self.movement = 0
            success = 0
            violation = None

            # trial start
            self.draw_pegs()
            self.draw_beads()
            self.win.flip()
            self.timer.reset()
            while True:
                self.choosing_bead()

                # waiting for movement
                self.moving_bead()
                # get first movement time
                if first:
                    first_movement_time = self.timer.getTime()
                    first = False
                # check if it is correct
                if self.pegSeat == self.goal:
                    completeRT = self.timer.getTime()
                    success = 1
                    # log data
                    trial_data = {
                        'running': 'experiment',
                        'trial': idx,
                        'success': success,
                        'movement': self.movement,
                        'first_movement_time': first_movement_time,
                        'completeRT': completeRT,
                        'violation': violation
                    }
                    logData(self.logfile, trial_data)
                    # clear the display
                    self.win.flip()
                    self.draw_pegs()
                    self.draw_beads()
                    self.fb[1].draw()
                    self.win.flip()
                    core.wait(self.constants.ITI)
                    break
                else:
                    if self.movement < self.constants.MAXMOVE and self.timer.getTime(
                    ) < self.constants.MAXTIME:
                        pass
                    else:
                        # clear the display
                        self.win.flip()
                        if self.movement >= self.constants.MAXMOVE:
                            violation = 'movement'
                            self.fb[0].draw()
                        elif self.timer.getTime() >= self.constants.MAXTIME:
                            violation = 'time'
                            self.fb[2].draw()
                        completeRT = self.timer.getTime()
                        success = 0
                        # log data
                        trial_data = {
                            'running': 'practice',
                            'trial': idx,
                            'success': success,
                            'movement': self.movement,
                            'first_movement_time': first_movement_time,
                            'completeRT': completeRT,
                            'violation': violation
                        }
                        logData(self.logfile, trial_data)
                        self.draw_pegs()
                        self.draw_beads()
                        self.win.flip()
                        core.wait(self.constants.ITI)
                        self.pegSeat = self.goal
                        break

    # prepare an dict for recording the position of beads (practice)
    def pegSeat_refresh(self, INPUT):
        self.pegSeat = {}
        for i in range(len(self.constants.SUBJBEADPOS)):
            self.pegSeat[i] = None
        for col in self.constants.BEADCOLS.keys():
            for seat in self.pegSeat.keys():
                if INPUT[seat] == col:
                    self.pegSeat[seat] = col
        return (self.pegSeat)

    def draw_beads(self):
        for col in self.constants.BEADCOLS.keys():
            # subject's
            for seat, value in self.pegSeat.items():
                if col == value:
                    self.subjAllBeads[col].setPos(
                        self.constants.SUBJBEADPOS[seat])
                    self.subjAllBeads[col].draw()
            # goal
            for seat, value in self.goal.items():
                if col == value:
                    self.subjAllBeads[col].setPos(
                        self.constants.GOALBEADPOS[seat])
                    self.subjAllBeads[col].draw()

    # create a function to draw pegs
    def draw_pegs(self):
        for i, peg in enumerate(self.pegs):
            # subject's
            peg.setPos(self.constants.SUBJPEGPOS[i])
            peg.draw()
            self.ground.setPos(self.constants.SUBJGROUNDPOS)
            self.ground.draw()
            # goal
            peg.setPos(self.constants.GOALPEGPOS[i])
            peg.draw()
            self.ground.setPos(self.constants.GOALGROUNDPOS)
            self.ground.draw()

    # create a function to draw buttons
    def draw_buttons(self):
        for x in self.constants.SUBJBUTPOS:
            self.button.setPos(x)
            self.button.draw()

    # create function to wait for choosing beads
    def choosing_bead(self):
        pressed_bead = None
        pressed_bead_old_seat = None
        Working = True
        cond = 0
        # set mouse position to an impossible position for target to appear
        self.mouse.setPos([-0.5 * x for x in self.constants.DISPSIZE])
        while Working:
            for seat, bead in {
                    key: value
                    for key, value in self.pegSeat.items() if value != None
            }.items():
                self.draw_pegs()
                self.draw_beads()
                self.win.flip()
                mouse_pos = self.mouse.getPos()
                if cdist([mouse_pos], [self.constants.SUBJBEADPOS[seat]
                                       ]) <= self.constants.BEADR:
                    if seat in [2, 4, 5]:
                        cond = 1
                    elif (seat == 3 and self.pegSeat[4] is None):
                        cond = 1
                    elif seat == 2:
                        cond = 1
                    elif (seat == 1 and self.pegSeat[2] is None):
                        cond = 1
                    elif (seat == 0 and self.pegSeat[1] is None
                          and self.pegSeat[2] is None):
                        cond = 1
                    else:
                        pass

                    if cond:
                        pressed_bead = bead
                        pressed_bead_old_seat = seat
                        Working = False

        self.pressed_bead = pressed_bead
        self.pressed_bead_old_seat = pressed_bead_old_seat

    # draw ring on chosen bead
    def draw_ring(self, where):
        self.ring.setPos(self.constants.SUBJBEADPOS[where])
        self.ring.draw()

    # create function for moving bead
    def moving_bead(self):
        waiting_for_movement = True
        move_cond = None
        temp = {
            key: value
            for key, value in self.pegSeat.items() if value == None
        }
        # set mouse position to an impossible position for target to appear
        self.mouse.setPos([-0.5 * x for x in self.constants.DISPSIZE])
        # check the state of pegs
        while waiting_for_movement:
            mouse_pos = self.mouse.getPos()
            for i, x in enumerate(self.constants.SUBJBUTPOS):
                if cdist([mouse_pos], [x]) <= self.constants.BUTSIZE:
                    # check if the movement is in the same peg
                    # if so, don't add movement
                    if i == 0 and self.pressed_bead_old_seat <= 2:
                        move_cond = 0
                    elif (i == 1 and (self.pressed_bead_old_seat == 3
                                      or self.pressed_bead_old_seat == 4)):
                        move_cond = 0
                    elif i == 2 and self.pressed_bead_old_seat == 5:
                        move_cond = 0
                    # if it is the first peg and it's not full
                    elif i == 0 and min(temp.keys()) <= 2:
                        move_cond = 1
                        new_pos = min(temp.keys())
                    # if it's second peg and not full
                    elif i == 1 and 3 in temp:
                        move_cond = 1
                        new_pos = 3
                    elif i == 1 and 4 in temp:
                        move_cond = 1
                        new_pos = 4
                    # if it is the third peg and it's not full
                    elif i == 2 and max(temp.keys()) == 5:
                        move_cond = 1
                        new_pos = 5
                    # if the peg is full, ignore
                    else:
                        move_cond = None

                self.draw_pegs()
                self.draw_beads()
                if move_cond:
                    self.pegSeat[self.pressed_bead_old_seat] = None
                    self.pegSeat[new_pos] = self.pressed_bead
                    self.movement += 1
                    waiting_for_movement = False
                    break
                elif move_cond is not None:
                    waiting_for_movement = False
                    break
                else:
                    self.draw_ring(self.pressed_bead_old_seat)
                    self.draw_buttons()

                self.win.flip()


class WCST:
    def __init__(self, subject_nr):
        self.constants = constants.WCSTConstants()
        self.info = {
            'subject_nr': subject_nr,
            'experiment_date': self.constants.DATE
        }

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=self.constants.DISPSIZE,
            units='pix',
            color=self.constants.BGC,
            fullscr=True,
            allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.mouse.setVisible(True)
        # create Clock for timer
        self.timer = core.Clock()

        # instruction
        self.instStim = visual.ImageStim(self.win, image=self.constants.INST)
        self.pracEnd = visual.ImageStim(self.win, image=self.constants.PRACEND)
        self.bye = visual.ImageStim(self.win, image=self.constants.BYE)
        self.conBut = visual.ImageStim(
            self.win, image=self.constants.CON, pos=(0, -700))
        self.change = visual.ImageStim(
            self.win, image=self.constants.CHANGE, pos=(0, -800))

        # stimuli
        self.allStim = {}
        for key, stim in self.constants.STIMULI.items():
            stimXpos = self.constants.STIMXPOS[int(stim['number'])]
            stimYpos = self.constants.STIMYPOS
            self.allStim[key] = visual.ImageStim(
                self.win, image=stim['stimPath'], pos=(stimXpos, stimYpos))
        # response cards
        self.allResponse = {}
        for response in self.constants.RESPONSES:
            self.allResponse[tuple(response.items())] = visual.ImageStim(
                self.win,
                image=response['responsePath'],
                pos=self.constants.RESPOS)
        # preparation of feedback
        self.fb = {}
        self.fb[0] = visual.ImageStim(self.win, image=self.constants.INCORRECT)
        self.fb[1] = visual.ImageStim(self.win, image=self.constants.CORRECT)

        # data file
        self.logfile = data.ExperimentHandler(
            name='WCST_subject-{}'.format(self.info['subject_nr']),
            extraInfo=self.info,
            autoLog=False)

    def run(self):
        # generate the random seed
        np.random.seed()
        # prepare the experiment
        self.prepare()

        # show instructions
        self.instStim.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break

        self.practice()
        self.experiment()

        # show ending of the experiment
        self.bye.draw()
        self.win.flip()
        while True:
            if any(self.mouse.getPressed()):
                break
        # end the experiment
        self.logfile.close()
        self.logfile.saveAsWideText(
            fileName=os.path.join(
                constants.EFKitConstants.DATADIR,
                'WCST_subject-{}.csv'.format(self.info['subject_nr'])),
            delim=',')
        self.win.close()

    def practice(self):
        again = True
        while again:
            # create some practice trials
            self.constants.PRACTICE = np.random.choice(
                self.constants.RESPONSES, 12)
            np.random.shuffle(self.constants.PRACTICE)
            # setup a counter
            self.counter = {'shape': 0, 'color': 0, 'number': 0}
            # # get a copy in advance
            # self.remains = self.constants.SORTING[:]
            self.flag = False
            self.old_sorting = None
            self.former = None
            self.shift = 0
            self.trial = 0
            self.mFail = 0
            self.success = 0
            self.first = True
            going = True
            while going:
                for response in self.constants.PRACTICE:
                    trial_data = self.trial_seq(response)
                    trial_data['running'] = 'practice'
                    logData(self.logfile, trial_data)
                    # check the number of shifts
                    if self.success == self.constants.SHIFTNUM:
                        self.counter[self.sorting] += 1
                        self.shift += 1
                        # reset
                        self.success = 0
                        if self.shift < self.constants.SHIFTNUM:
                            new = np.random.choice([
                                others
                                for others, counts in self.counter.items()
                                if (others is not self.sorting) and (counts < 2)
                            ])
                            self.old_sorting = self.sorting
                            # change the rule
                            self.sorting = new

                            # ask to make change
                            self.change.draw()
                            # redraw stimuli
                            self.update_response()
                            # wait a bit
                            core.wait(self.constants.ITI)
                        else:
                            going = False
                            break
                else:
                    going = False

            # end of practice
            self.timer.reset()
            while self.timer.getTime() <= 15:
                self.pracEnd.draw()
                self.conBut.draw()
                self.win.flip()
                mouse_pos = self.mouse.getPos()
                if cdist([mouse_pos], [[0, -700]]) <= 168:
                    again = False
                    break

    def experiment(self):
        # randomization
        np.random.shuffle(self.constants.RESPONSES)
        # setup a counter
        self.counter = {'shape': 0, 'color': 0, 'number': 0}
        # # get a copy in advance
        # self.remains = self.constants.SORTING[:]
        self.flag = False
        self.old_sorting = None
        self.former = None
        self.shift = 0
        self.trial = 0
        self.mFail = 0
        self.success = 0
        self.first = True
        going = True
        while going:
            for response in self.constants.RESPONSES:
                trial_data = self.trial_seq(response)
                trial_data['running'] = 'experiment'
                logData(self.logfile, trial_data)
                # check the number of shifts
                if self.success == self.constants.SHIFTNUM:
                    self.counter[self.sorting] += 1
                    self.shift += 1
                    # reset
                    self.success = 0
                    if self.shift < self.constants.SHIFTNUM:
                        new = np.random.choice([
                            others for others, counts in self.counter.items()
                            if (others is not self.sorting) and (counts < 2)
                        ])
                        self.old_sorting = self.sorting
                        # change the rule
                        self.sorting = new

                        # ask to make change
                        self.change.draw()
                        # redraw stimuli
                        self.update_response()
                        # wait a bit
                        core.wait(self.constants.ITI)
                    else:
                        going = False
                        break
            else:
                going = False

    def trial_seq(self, response):
        self.timer.reset()
        self.trial += 1
        # update response
        self.update_response(response)
        core.wait(0.5)  # why??????????
        chosen, RT = self.choose_card(response)
        # first respose will always be correct sorting
        if self.first:
            match = [
                k for k, v in self.constants.STIMULI[chosen].items()
                if v in response.values()
            ]
            # if not matched
            if not match:
                self.sorting = None
            # if match is found, set it to the sorting rule
            else:
                self.sorting = match[0]
                self.first = False

        values = self.check_response(chosen, response)
        # record the current way of categorizing
        self.former = values['current']

        if values['acc']:
            self.success += 1
        else:
            self.success = 0
            self.flag = True  # flag the error for Nelson's errors

        # check if faied to maintain a set
        if self.success >= 3 and not values['acc']:
            self.mFail = 1

        # redraw everything and give feedbacks
        self.move_response(response, chosen, values['acc'])
        core.wait(self.constants.FBTIME)
        # return trial data
        trial_data = {
            'RT': RT,
            'sorting': self.sorting,
            'old_sorting': self.old_sorting,
            'former': self.former,
            'acc': values['acc'],
            'success': self.success,
            'fail_to_maintain_the_rule': self.mFail,
            'stim_shape': self.constants.STIMULI[chosen]['shape'],
            'stim_number': self.constants.STIMULI[chosen]['number'],
            'stim_color': self.constants.STIMULI[chosen]['color'],
            'response_shape': response['shape'],
            'response_number': response['number'],
            'response_color': response['color'],
            'shift': self.shift,
            'otherError': values['error'],
            'MError': values['MError'],
            'NError': values['NError']
        }

        return trial_data

    def update_response(self, response=None):
        for v in self.allStim.values():
            v.draw()
        if response is not None:
            self.allResponse[tuple(response.items())].draw()
        self.win.flip()

    def move_response(self, response, where, fb=None):
        for v in self.allStim.values():
            v.draw()
        # move the response card
        self.allResponse[tuple(response.items())].setPos(
            [self.constants.RESDECXPOS[where], self.constants.RESDECYPOS])
        self.allResponse[tuple(response.items())].draw()
        if fb in self.fb:
            self.fb[fb].setPos(
                [self.constants.RESDECXPOS[where], self.constants.FBY])
            self.fb[fb].draw()
        self.win.flip()
        # reset the response card
        self.allResponse[tuple(response.items())].setPos(self.constants.RESPOS)

    def choose_card(self, response):
        # set mouse position to an impossible position for target to appear
        self.mouse.setPos([-0.5 * x for x in self.constants.DISPSIZE])
        while True:
            self.update_response(response)  # to check mouse pos
            mouse_pos = self.mouse.getPos()
            for k in self.constants.STIMULI:
                if cdist([mouse_pos], [self.allStim[k].pos]) <= max(
                        self.allStim[k].size):
                    RT = self.timer.getTime()
                    return (k, RT)

    def check_response(self, chosen, response):
        acc = 0
        success = 0
        error = 0
        MError = 0
        NError = 0
        match = [
            k for k, v in self.constants.STIMULI[chosen].items()
            if v in response.values()
        ]
        if not match:
            error = 1
            current = None
        else:
            # get value instead of list
            current = match[0]
            if current == self.sorting:
                acc = 1
                success = 1
            else:
                # check if perservative (Milner)
                if current == self.old_sorting:
                    MError = 1
                # check if it is a Nelson's error
                if self.flag and current == self.former:
                    NError = 1

        return {
            'current': current,
            'acc': acc,
            'success': success,
            'error': error,
            'MError': MError,
            'NError': NError
        }


# Timeout
class TimeOut:
    def __init__(self):
        pass

    def prepare(self):
        # create a Window to control the monitor
        self.win = visual.Window(
            size=[1200, 1200], units='pix', fullscr=True, allowGUI=False)
        # create a Mouse to collect mouse reponses
        self.mouse = event.Mouse(self.win)
        self.stim = visual.ImageStim(
            self.win,
            image=os.path.join(constants.EFKitConstants.STIMDIR,
                               'timeout.png'))
        self.conBut = visual.ImageStim(
            self.win,
            image=os.path.join(constants.EFKitConstants.STIMDIR,
                               'continue.png'),
            pos=(0, -700))

    def run(self):
        self.prepare()
        while True:
            self.stim.draw()
            self.conBut.draw()
            self.win.flip()
            if self.mouse.isPressedIn(self.conBut):
                break

        self.win.close()

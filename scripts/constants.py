# -*- coding: utf-8 -*-
import csv
import os
import math
from datetime import datetime


###############################################################################
# Global
###############################################################################
class EFKitConstants:
    # get the directory
    SCRIPTDIR = os.path.abspath(os.path.dirname(__file__))
    ROOTDIR = os.path.abspath(os.path.join(SCRIPTDIR, os.pardir))
    DATADIR = os.path.join(ROOTDIR, 'data')
    STIMDIR = os.path.join(ROOTDIR, 'stimuli')
    if not os.path.isdir(DATADIR):
        os.mkdir(DATADIR)
    # display resolution
    DISPSIZE = (2736, 1824)
    # experiment date
    DATE = str(datetime.now())
    # background and foreground color
    FGC = (1, 1, 1)
    BGC = (-1, -1, -1)

    def __init__(self, size=None):
        if size is not None:
            self.DISPSIZE = size


###############################################################################
# No-Go
###############################################################################
class NogoConstants(EFKitConstants):
    # fixation duration
    FIXTIME = 0.2
    # fixation size
    FIXSIZE = 48
    # stimulus duration
    STIMTIME = 0.6
    # inter-trial interval
    ITI = 0.5
    # feedback duration
    FBTIME = 0.6
    # practice ratio
    PRACRATIO = 0.166

    # instruction
    INST = os.path.join(EFKitConstants.STIMDIR, 'nogo/instruction.png')
    PRACEND0 = os.path.join(EFKitConstants.STIMDIR, 'nogo/pracEnd0.png')
    PRACEND1 = os.path.join(EFKitConstants.STIMDIR, 'nogo/pracEnd1.png')
    CON = os.path.join(EFKitConstants.STIMDIR, 'nogo/continue.png')
    BYE = os.path.join(EFKitConstants.STIMDIR, 'nogo/bye.png')
    # stimulus
    INFREQGO = os.path.join(EFKitConstants.STIMDIR, 'nogo/infrequent_go.png')
    FREQGO = os.path.join(EFKitConstants.STIMDIR, 'nogo/frequent_go.png')
    NOGO = os.path.join(EFKitConstants.STIMDIR, 'nogo/nogo.png')
    # the number of trials
    STIMS = {'infrequent_go': 24, 'frequent_go': 72, 'nogo': 24}
    CORRECT = os.path.join(EFKitConstants.STIMDIR, 'nogo/correct.png')
    INCORRECT = os.path.join(EFKitConstants.STIMDIR, 'nogo/incorrect.png')
    # define the threshold for practice
    THRESHOLD = 0.7

    def __init__(self, size=None):
        """Inherit all the attributes

        """
        super().__init__(size)


###############################################################################
# RVP
###############################################################################
class RVPConstants(EFKitConstants):

    INST = os.path.join(EFKitConstants.STIMDIR, 'rvp/inst.png')
    PRACEND = os.path.join(EFKitConstants.STIMDIR, 'rvp/pracEnd.png')
    REST = os.path.join(EFKitConstants.STIMDIR, 'rvp/resting.png')
    BYE = os.path.join(EFKitConstants.STIMDIR, 'rvp/bye.png')
    CON = os.path.join(EFKitConstants.STIMDIR, 'rvp/continue.png')
    MOUSE = os.path.join(EFKitConstants.STIMDIR, 'rvp/mouse.png')
    CORRECT = os.path.join(EFKitConstants.STIMDIR, 'rvp/correct.png')
    INCORRECT = os.path.join(EFKitConstants.STIMDIR, 'rvp/incorrect.png')
    # define the number of full sequences
    SEQNUM = 40
    # define the number of target sequences
    TARNUM = 10
    # define the range of random numbers
    NUMBERS = range(2, 10)
    # define max waiting time for click (i.e., duration of each digit)
    STIMTIME = 0.5
    # define fontsize
    FONTSIZE = 120
    # define the number of blocks
    BLOCK = 3

    def __init__(self, size=None):
        """Inherit all the attributes

        """
        super().__init__(size)


###############################################################################
# SWM
###############################################################################
class SWMConstants(EFKitConstants):
    BGC = (1, 1, 1)
    # define the margin (to be discussed...)
    WMARGIN = 140
    HMARGIN = 70
    # define the cover trees, birds and unfound leaf
    # i'm lazy.
    COVERTREES = []
    BIRDS = []
    for num in range(1, 9):
        coverName = os.path.join(EFKitConstants.STIMDIR,
                                 'swm/coverTree_{}.png'.format(str(num)))
        birdName = os.path.join(EFKitConstants.STIMDIR,
                                'swm/bird_{}.png'.format(str(num)))
        COVERTREES.append(coverName)
        BIRDS.append(birdName)

    COVERPRAC = os.path.join(EFKitConstants.STIMDIR, 'swm/coverTree_prac.png')
    BIRDPRAC = os.path.join(EFKitConstants.STIMDIR, 'swm/bird_prac.png')
    LEAF = os.path.join(EFKitConstants.STIMDIR, 'swm/unfoundLeaf.png')
    INST1 = os.path.join(EFKitConstants.STIMDIR, 'swm/inst1.png')
    INST2 = os.path.join(EFKitConstants.STIMDIR, 'swm/inst2.png')
    INST3 = os.path.join(EFKitConstants.STIMDIR, 'swm/inst3.png')
    BYE = os.path.join(EFKitConstants.STIMDIR, 'swm/bye.png')
    PRACEND = os.path.join(EFKitConstants.STIMDIR, 'swm/pracEnd.png')

    # define the size of cover trees
    COVERSIZE = (160, 160)
    # define the size of target
    TARSIZE = (120, 120)
    # define the minimum distance ratio between two covers
    MINRATIO = 1.42
    # define the time between each flip
    FLIPTIME = 0.5
    # define the time between each trial
    ITI = 1
    # define the numbers of boxes
    BOXNUMS = [3, 4, 6, 7, 8, 9]
    PRACIDX = 2
    # define the maximum time
    TIMEOUT = 60
    # define the number of trials
    TRIALREPEATS = 2

    def __init__(self, size=None):
        """Inherit all the attributes

        """
        super().__init__(size)


###############################################################################
# TOL
###############################################################################
class TOLConstants(EFKitConstants):
    INST = os.path.join(EFKitConstants.STIMDIR, 'tol/inst.png')
    PRACEND = os.path.join(EFKitConstants.STIMDIR, 'tol/pracEnd.png')
    BYE = os.path.join(EFKitConstants.STIMDIR, 'tol/bye.png')
    CON = os.path.join(EFKitConstants.STIMDIR, 'tol/continue.png')
    FB0 = os.path.join(EFKitConstants.STIMDIR, 'tol/fb0.png')
    FB1 = os.path.join(EFKitConstants.STIMDIR, 'tol/fb1.png')
    FB2 = os.path.join(EFKitConstants.STIMDIR, 'tol/fb2.png')

    # define the size of beads
    BEADR = 80
    # define the color of peg
    PEGCOL = '#CD853F'
    # define the length and thickness of ground
    GROUNDWIDTH = 850
    GROUNDHEIGHT = 40
    # define the position of ground
    GOALGROUNDPOS = (200, 300)
    # define the position of goal pegs
    HALFGOALGROUND = math.ceil(0.5 * GROUNDHEIGHT + GOALGROUNDPOS[1])
    GOALPEGPOS = {
        0: (-200, math.ceil(HALFGOALGROUND + 3 * BEADR)),
        1: (200, math.ceil(HALFGOALGROUND + 2 * BEADR)),
        2: (600, math.ceil(HALFGOALGROUND + BEADR))
    }

    # define the position of beads
    GOALBEADPOS = {
        0: (-200, math.ceil(HALFGOALGROUND + 1 * BEADR)),
        1: (-200, math.ceil(HALFGOALGROUND + 3 * BEADR)),
        2: (-200, math.ceil(HALFGOALGROUND + 5 * BEADR)),
        3: (200, math.ceil(HALFGOALGROUND + BEADR)),
        4: (200, math.ceil(HALFGOALGROUND + 3 * BEADR)),
        5: (600, math.ceil(HALFGOALGROUND + BEADR))
    }
    # define the position of ground
    SUBJGROUNDPOS = (200, -400)
    # define the position of three pegs
    HALFSUBHGROUND = math.ceil(0.5 * GROUNDHEIGHT + SUBJGROUNDPOS[1])
    SUBJPEGPOS = {
        0: (-200, math.ceil(HALFSUBHGROUND + 3 * BEADR)),
        1: (200, math.ceil(HALFSUBHGROUND + 2 * BEADR)),
        2: (600, math.ceil(HALFSUBHGROUND + BEADR))
    }
    # define the possible position of beadsh
    SUBJBEADPOS = {
        0: (-200, math.ceil(HALFSUBHGROUND + BEADR)),
        1: (-200, math.ceil(HALFSUBHGROUND + 3 * BEADR)),
        2: (-200, math.ceil(HALFSUBHGROUND + 5 * BEADR)),
        3: (200, math.ceil(HALFSUBHGROUND + BEADR)),
        4: (200, math.ceil(HALFSUBHGROUND + 3 * BEADR)),
        5: (600, math.ceil(HALFSUBHGROUND + BEADR))
    }

    # define the position of buttons
    SUBJBUTPOS = [(-200, -600), (200, -600), (600, -600)]
    # define the width and length of pegs
    PEGWIDTH = 50
    PEGHEIGHT = [
        math.ceil(6 * BEADR),
        math.ceil(4 * BEADR),
        math.ceil(2 * BEADR)
    ]

    # define the color of beads
    # red, green, blue
    BEADCOLS = {'R': (1, -1, -1), 'G': (-1, 1, -1), 'B': (-1, -1, 1)}
    # practice starting position
    PRACSTARTING = {0: 'G', 1: None, 2: None, 3: 'B', 4: None, 5: 'R'}
    # starting position
    STARTING = {0: 'G', 1: 'R', 2: None, 3: 'B', 4: None, 5: None}
    # define the size of buttons for each peg
    BUTSIZE = 100
    # define the color of buttons
    BUTCOL = (1, 1, 1)
    # define clickGap
    clickGap = 0.1
    # define ITI
    ITI = 2
    # define max allowable time (in seconds) in each trial
    MAXTIME = 120
    # define the max allowable movements
    MAXMOVE = 20
    # create practice trials
    PRACTICE = {
        0: {
            0: 'G',
            1: None,
            2: None,
            3: 'B',
            4: 'R',
            5: None
        },
        1: {
            0: 'G',
            1: 'R',
            2: None,
            3: 'B',
            4: None,
            5: None
        },
        2: {
            0: 'G',
            1: 'R',
            2: None,
            3: None,
            4: None,
            5: 'B'
        },
        3: {
            0: 'G',
            1: None,
            2: None,
            3: 'R',
            4: None,
            5: 'B'
        },
        4: {
            0: None,
            1: None,
            2: None,
            3: 'R',
            4: 'G',
            5: 'B'
        },
        5: {
            0: 'B',
            1: None,
            2: None,
            3: 'R',
            4: 'G',
            5: None
        },
        6: {
            0: 'B',
            1: 'G',
            2: None,
            3: None,
            4: None,
            5: 'R'
        },
        7: {
            0: 'B',
            1: None,
            2: None,
            3: 'G',
            4: 'R',
            5: None
        },
        8: {
            0: 'B',
            1: 'R',
            2: None,
            3: None,
            4: None,
            5: 'G'
        },
        9: {
            0: 'G',
            1: None,
            2: None,
            3: 'R',
            4: 'B',
            5: None
        }
    }

    # define all the trials
    GOALS = {
        0: {
            0: 'G',
            1: 'B',
            2: None,
            3: 'R',
            4: None,
            5: None
        },
        1: {
            0: 'R',
            1: None,
            2: None,
            3: 'B',
            4: None,
            5: 'G'
        },
        2: {
            0: 'G',
            1: None,
            2: None,
            3: 'R',
            4: 'B',
            5: None
        },
        3: {
            0: 'R',
            1: 'G',
            2: None,
            3: 'B',
            4: None,
            5: None
        },
        4: {
            0: 'B',
            1: None,
            2: None,
            3: 'R',
            4: 'G',
            5: None
        },
        5: {
            0: None,
            1: None,
            2: None,
            3: 'R',
            4: 'B',
            5: 'G'
        },
        6: {
            0: 'B',
            1: None,
            2: None,
            3: 'R',
            4: None,
            5: 'G'
        },
        7: {
            0: 'R',
            1: 'G',
            2: None,
            3: None,
            4: None,
            5: 'B'
        },
        8: {
            0: 'B',
            1: 'G',
            2: None,
            3: 'R',
            4: None,
            5: None
        },
        9: {
            0: 'R',
            1: None,
            2: None,
            3: 'G',
            4: 'B',
            5: None
        },
        10: {
            0: 'B',
            1: 'G',
            2: None,
            3: None,
            4: None,
            5: 'R'
        },
        11: {
            0: 'R',
            1: None,
            2: None,
            3: 'G',
            4: None,
            5: 'B'
        },
        12: {
            0: 'B',
            1: 'R',
            2: None,
            3: 'G',
            4: None,
            5: None
        },
        13: {
            0: 'B',
            1: 'R',
            2: 'G',
            3: None,
            4: None,
            5: None
        },
        14: {
            0: 'B',
            1: None,
            2: None,
            3: 'G',
            4: None,
            5: 'R'
        }
    }

    def __init__(self, size=None):
        """Inherit all the attributes

        """
        super().__init__(size)


###############################################################################
# Wisconsin Card Sorting
###############################################################################
class WCSTConstants(EFKitConstants):
    # define instructions
    INST = os.path.join(EFKitConstants.STIMDIR, 'wcst/instruction.png')
    PRACEND = os.path.join(EFKitConstants.STIMDIR, 'wcst/pracEnd.png')
    BYE = os.path.join(EFKitConstants.STIMDIR, 'wcst/bye.png')
    CORRECT = os.path.join(EFKitConstants.STIMDIR, 'wcst/correct.png')
    INCORRECT = os.path.join(EFKitConstants.STIMDIR, 'wcst/incorrect.png')
    CHANGE = os.path.join(EFKitConstants.STIMDIR, 'wcst/rule-change.png')
    CON = os.path.join(EFKitConstants.STIMDIR, 'wcst/continue.png')
    # define the duration of feedback
    FBTIME = 2
    # define the duration of hint
    ITI = 5

    # read all the response cards
    # create a dict for all the paths of stimulus
    RESCSV = os.path.join(EFKitConstants.STIMDIR, 'wcst/responseCards.csv')
    RESPONSES = []
    with open(RESCSV) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            response = {
                'shape': row['shape'],
                'color': row['color'],
                'number': row['number']
            }
            responseName = os.path.join(
                EFKitConstants.STIMDIR, 'wcst/{0}_{1}_{2}.png'.format(
                    row['shape'], row['color'], row['number']))
            response['responsePath'] = responseName
            RESPONSES.append(response)

    RESPONSES = [item for item in RESPONSES for i in range(2)]

    # define the change criterior and also the maximum shifts
    SHIFTNUM = 6
    # create a list for six sorting categories
    SORTING = ['shape', 'color', 'number']

    # define stimuli(i.e., the four cards on top)
    STIMCSV = os.path.join(EFKitConstants.STIMDIR, 'wcst/stimulusCards.csv')
    STIMULI = {}
    with open(STIMCSV) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            stimulus = {
                'shape': row['shape'],
                'color': row['color'],
                'number': row['number']
            }
            stimulusName = os.path.join(
                EFKitConstants.STIMDIR, 'wcst/{0}_{1}_{2}.png'.format(
                    row['shape'], row['color'], row['number']))
            stimulus['stimPath'] = stimulusName
            STIMULI[int(row['stimOrder'])] = stimulus

    def __init__(self, size=None):
        """Inherit all the attributes

        """
        # define the position of stimuli
        self.STIMXPOS = {
            1: math.ceil(-0.3 * self.DISPSIZE[0]),
            2: math.ceil(-0.1 * self.DISPSIZE[0]),
            3: math.ceil(0.1 * self.DISPSIZE[0]),
            4: math.ceil(0.3 * self.DISPSIZE[0])
        }
        self.STIMYPOS = math.ceil(0.2 * self.DISPSIZE[1])
        # define the position of response cards
        self.RESPOS = (0, math.ceil(-0.3 * self.DISPSIZE[1]))
        # define the position of response/stimuli pair
        self.RESDECXPOS = {key: value for key, value in self.STIMXPOS.items()}
        self.RESDECYPOS = math.ceil(self.STIMYPOS - 410)
        # feedback position
        self.FBY = self.RESDECYPOS - 400
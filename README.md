# EFKit

## 功能

+ 電腦化執行功能測驗
+ 測驗結果描述性統計分析

## 發展團隊

國立臺灣大學嬰兒語言實驗室

## 授權

GPL v3.0

## 安裝流程

### 必要

1. 下載並解壓縮EFKit-2.zip
2. 檢查檔案結構
    ```text
    EFKit-2
    │   COPYING
    │   EFKit_Describer.bat
    │   EFKit_Launcher.bat
    │   EFKit.ico
    │   README.md
    │
    └───data
    └───scripts
    │   │   __init__.py
    │   │   app.py
    │   │   constants.py
    │   │   experiment.py
    │   │   qtdescriber.py
    │   │   utility.py
    │
    └───stimuli
    │   │   continue.png
    │   │   timeout.png
    │   │
    │   └───nogo
    │   └───rvp
    │   └───swm
    │   └───tol
    │   └───wcst
    └───summaries
    ```
